# -*- coding:utf-8 -*-

from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from smallurl.apps.smurl.models import Urls, UrlItems

class UrlForm(forms.Form):
    url = forms.CharField(label='URL', max_length=200, widget=forms.TextInput(),required=True)
    def __init__(self, user=None, *args, **kwargs):
        if user.is_authenticated():
            self.username = user
        else:
            try:
                anonymous = User.objects.get(username='anonymous')
            except User.DoesNotExist:
                anonymous = User.objects.create_user('anonymous', 'anonymous@anonymous.com', 'anonymous')
                anonymous.is_active = False
                anonymous.save()
            self.username = anonymous
        super(UrlForm, self).__init__(*args, **kwargs)

    def clean_url(self):
        validate = URLValidator(verify_exists=True)
        value = self.cleaned_data["url"]
        if value:
            try:
                validate(value)
            except ValidationError:
                raise forms.ValidationError("Error validation URL")
        return self.cleaned_data["url"]

    def save(self):
        url, created = Urls.objects.get_or_create(url=self.cleaned_data["url"])
        return UrlItems.objects.create(url=url, username=self.username)

class UrlsAdminForm(forms.ModelForm):
    class Meta:
        model = Urls

class UrlItemsAdminForm(forms.ModelForm):
    class Meta:
        model = UrlItems
        fields = ['short_url', 'username', 'hit_counter', ]
