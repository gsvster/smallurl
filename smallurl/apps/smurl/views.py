# -*- coding:utf-8 -*-

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from smallurl.utils.views import render_to
from smallurl.apps.smurl.forms import UrlForm
from smallurl.apps.smurl.models import UrlItems

def redirect_short_url(request, short_url):
    url = get_object_or_404(UrlItems, short_url__iexact=short_url)
    url.hit_counter += 1
    url.save()
    return redirect(url.url.url)

@render_to('')
def homepage(request, template="smurl/homepage.html"):
    new_obj = None
    if request.method == "POST":
        form = UrlForm(request.user, request.POST)
        if form.is_valid():
            new_obj = form.save()
    else:
        form = UrlForm(request.user)

    return {
        'form':form,
        'short_url':new_obj,
    }, template

