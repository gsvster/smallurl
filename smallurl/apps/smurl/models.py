# -*- coding:utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from random import choice, seed
seed()

def random_string(char_length=4, digit_length=4):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    digit = "1234567890"
    pass_str_char = ''.join([choice(chars) for i in range(char_length)])
    pass_str_digit = ''.join([choice(digit) for i in range(digit_length)])
    return pass_str_char + pass_str_digit

class Urls(models.Model):
    url = models.URLField(verbose_name='Full URL', max_length=300)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True)

    class Meta:
        verbose_name = 'Url'
        verbose_name_plural = 'Urls'
        ordering = ('created',)

    def __unicode__(self):
        return u'%s' % self.url

class UrlItems(models.Model):
    url = models.ForeignKey(Urls)
    short_url = models.CharField(verbose_name='Short URL', max_length=8, blank=True, null=True)
    username = models.ForeignKey(User, blank=False, null=False, verbose_name='Username')
    hit_counter = models.IntegerField(verbose_name='Counter', default=0)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True)
    last_redirect = models.DateTimeField(verbose_name='Last redirect', auto_now_add=True)

    class Meta:
        verbose_name = 'Url item'
        verbose_name_plural = 'Url items'
        ordering = ('created',)

    def __unicode__(self):
        return u'%s' % self.short_url

    def save(self, *args, **kwargs):
        if not self.short_url:
            self.short_url = random_string()
        super(UrlItems, self).save(*args, **kwargs)

    def shor_absolute_url(self):
        return "http://%(site)s/%(rand_url)s" % {'site':Site.objects.get_current(), 'rand_url':self.short_url}