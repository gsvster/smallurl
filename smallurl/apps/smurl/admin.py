# -*- coding:utf-8 -*-

from django.contrib import admin
from smallurl.apps.smurl.models import Urls, UrlItems
from smallurl.apps.smurl.forms import UrlsAdminForm, UrlItemsAdminForm

class UrlItemsAdminInline(admin.StackedInline):
    model = UrlItems
    form = UrlItemsAdminForm
    extra = 0

admin.site.register(Urls,
    inlines = [UrlItemsAdminInline],
    form = UrlsAdminForm,
)
