
from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^login/$', 'smallurl.apps.account.views.login', name="account_login"),
    url(r'^signup/$', 'smallurl.apps.account.views.signup', name="accountt_signup"),
    url(r'^my_links/$', 'smallurl.apps.account.views.my_links', name="accountt_my_links"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {"template_name": "account/logout.html"}, name="account_logout"),
)
