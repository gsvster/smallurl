# -*- coding:utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'smallurl.apps.smurl.views.homepage', name='homepage'),
    url(r'^(?P<short_url>\w{4}\d{4})/$', 'smallurl.apps.smurl.views.redirect_short_url', name='redirect_short_url'),
    url(r'^account/', include('smallurl.apps.account.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
